package br.com.mastertech.pagamento.clients;

import br.com.mastertech.pagamento.models.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;


@FeignClient(name = "cartao")
public interface CartaoClient {

    @GetMapping("/cartao/id/{id}")
    Optional<Cartao> getCartaoById(@PathVariable(name="id") Long id);
}
