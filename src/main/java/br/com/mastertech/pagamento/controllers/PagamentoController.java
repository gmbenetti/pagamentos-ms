package br.com.mastertech.pagamento.controllers;

import br.com.mastertech.pagamento.clients.CartaoClient;
import br.com.mastertech.pagamento.models.Cartao;
import br.com.mastertech.pagamento.models.Pagamento;
import br.com.mastertech.pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private CartaoClient cartaoClient;

    @PostMapping("/pagamento")
    public ResponseEntity<Pagamento> gravarPagamento(@RequestBody @Valid Pagamento pagamento){
        Optional<Cartao> cartaoOptional = cartaoClient.getCartaoById(pagamento.getCartaoId());
        if(cartaoOptional.isPresent()) {
            Pagamento pagamentoObjeto = pagamentoService.salvar(pagamento);
            return ResponseEntity.status(201).body(pagamentoObjeto);
        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/pagamentos/{idCartao}")
    public ResponseEntity<List<Pagamento>> consulta(@PathVariable(name="idCartao")Long idCartao){
        Optional<Cartao> cartaoOptional = cartaoClient.getCartaoById(idCartao);

        if(cartaoOptional.isPresent()) {
            Cartao cartao = cartaoOptional.get();
            List<Pagamento> pagamentos = pagamentoService.buscarPorCartao(cartao.getId());
            return ResponseEntity.status(200).body(pagamentos);
        }else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
