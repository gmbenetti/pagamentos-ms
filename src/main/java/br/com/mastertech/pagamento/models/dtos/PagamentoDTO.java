package br.com.mastertech.pagamento.models.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class PagamentoDTO {


    @JsonProperty("cartao_id")
    private Long cartaoId;
    private String descricao;
    private BigDecimal valor = new BigDecimal(2);

    public PagamentoDTO() {
    }

    public PagamentoDTO(Long cartao_id, String descricao, BigDecimal valor) {
        this.cartaoId = cartao_id;
        this.descricao = descricao;
        this.valor = valor;
    }

    public Long getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(Long cartaoId) {
        this.cartaoId = cartaoId;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
}
