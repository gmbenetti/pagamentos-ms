package br.com.mastertech.pagamento.repositories;


import br.com.mastertech.pagamento.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {
    List<Pagamento> findAllByCartaoId(Long cartaoId);
}