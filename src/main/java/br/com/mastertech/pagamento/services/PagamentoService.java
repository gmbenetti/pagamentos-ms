package br.com.mastertech.pagamento.services;

import br.com.mastertech.pagamento.models.Pagamento;
import br.com.mastertech.pagamento.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {
    @Autowired
    PagamentoRepository pagamentoRepository;

    public Pagamento salvar(Pagamento pagamento){
        Pagamento pagamentoObjeto = pagamentoRepository.save(pagamento);
        return pagamentoObjeto;
    }

    public List<Pagamento> buscarPorCartao(Long cartaoId) {
        List<Pagamento> pagamentos = pagamentoRepository.findAllByCartaoId(cartaoId);
        if(pagamentos.size() > 0){
            return pagamentos;
        }else{
            throw new RuntimeException("Cartão id " + cartaoId + " não possui pagamentos");
        }
    }
}
